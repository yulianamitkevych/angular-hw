import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {BoardService} from 'src/app/services/board.service';
import {jsPDF} from 'jspdf';
import * as html2pdf from 'html2pdf.js';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  constructor(
    public boardService: BoardService,
  ) {
  }

  ngOnInit(): void {
  }

  addColumn(event: string) {
    if (event) {
      this.boardService.addColumn(event);
    }
  }

  onExportClick(){
    const options = {
      filename: 'exported.pdf',
      image: {type: 'jpeg'},
      html2canvas: {},
      jsPDF: {orientation: 'landscape'}
    }

    const exportedContent: Element = document.getElementById('content');

    html2pdf()
      .from(exportedContent)
      .set(options)
      .save();
  }
}
